import json
import random
import os

def load_vocab():
    # Check to see for personal progress
    if os.path.isfile("personal.json") == True:
        with open("personal.json") as f:
            data = json.load(f)
    
    # If there's no personal progress, create one from the base vocab
    else:
        with open("vocab.json") as f:
            data = json.load(f)
        with open("personal.json", "w") as f:
            json.dump(data, f, indent=4, ensure_ascii=False)

    return data

def update_mastery(category, subdict):
    # Load in the existing json file
    with open("personal.json") as f:
        data = json.load(f)

    # As we pass the exact category and subdict, we can easily directly update the mastery
    data[category][subdict]["mastery"] = data[category][subdict]["mastery"] + 1

    # Update the json file
    with open("personal.json", "w") as f:
        json.dump(data, f, indent=4, ensure_ascii=False)

def check_mastery(category):
    # Load in the existing json file
    with open("personal.json") as f:
        data = json.load(f)

    # Check if each word has been mastered, if so add it to the list
    mastery_list = []
    for i in data[category]:
        if data[category][i]["mastery"] == 3:
            mastery_list.append(data[category][i]["mastery"])

    # If the len of the list of mastered words == len of all the words in the category, the category is mastered
    if len(mastery_list) == len(data[category]):
        return True
    else:
        return False

if __name__ == "__main__":
    category = input("你要学习什么？ ")
    
    while True:
        data = load_vocab()
        try:
            word_list = list(data[str(category)].items())
            word = random.choice(word_list)
            mastery = word[1]["mastery"]
            if mastery < 3:
                user_input = input("输入汉语: " + str(word[0]) + " ")
                correct_input = word[1]["character(s)"]
                pinyin = word[1]["pinyin"]
                if user_input == correct_input:
                    print("正确")
                    update_mastery(category, str(word[0]))
                else:
                    print("不正确的, 正确汉语是: ", correct_input, " (", pinyin, ")", sep="")
            else:
                # Check to see if all words have been mastered
                if check_mastery(category) == True:
                    print("You have obtained mastery on the subject of:", category)
                    break
                else:
                    pass         
        except KeyError:
            print("选择了无效的类别")